---
title: "About"
---

Hi. I am a human.  

## Why "Ultraviolet"?  

I will give you three reasons:  

1. The day this blog came online, I watched a movie in which the boy calls the girl "Ultraviolet" and I think that is beautiful.  

2. ["Ultraviolet (Light My Way)"][0] is a song by U2. I like their music.  

3. Ultraviolet radiation is a part of the electromagnetic spectrum with wavelength in the range of 100-400nm  


[0]: https://www.youtube.com/watch?v=TUD4CQT33w4