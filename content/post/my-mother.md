---
title: "My Mother"
date: 2021-07-07T19:16:09+05:30
--- 

My mother who has infinite tolerance.  
And taught me humility and patience.  
I love you more than you think, mother.  


.   
.   
.   
.   
.   
.  
.   
.   
.  
.   
.   
.  
.   
.   


Of course, she is pathetic in navigating life and taking decisions. I am mad at her for not letting father go overseas for work when I was in 7th grade on the pretense that _I would need him_. I can trace all failures as a family to that singular event.   

Again, cause and effect isn't linear but things would have turned out much different that way, maybe even for the better. 