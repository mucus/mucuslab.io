---
title: "Decision Making"
date: 2021-08-11T20:32:13+05:30
tags:
categories:
--- 

{{<toc>}}

## Topsy-turvy year  

Some very tough times are ahead. Important decisions have to be made by me. And it is one of those areas where I have sucked - miserably, uptil now. Due to the pandemic, admission procedure for virtually every worthwhile institute has been in shambles. Only now, are we seeing some freedom of operation and naturally, all those schools are in a state of panic. Everyone is desperate to get students and satisfy their vacancies.   

Normally, the JEE Advanced exam is conducted followed by other exams like NEST, IAT, ISI, UGEE etc. Now, because of the Advanced result in hand, students have a clear idea of where they stand and what options are available to them (if not the IITs). Consequently, they take an optimal decision wrt these other institutes. But this year, things are _so_ damn different. JEE Advanced has been delayed to October (fucking October!) and all these other institutes are scurrying to fill up their seats. How is this relevant?      

## The Offer  

Today, I received an offer letter from IISc Bangalore, the best research institute in the country, for their BSc (Research) Programme. Now, I know, I know. But instead of jumping with joy, I am worried as fucked.  

They say that students who ain't good enough face a lot of trouble getting into colleges, getting a job and so on. But they are all wrong. This argument is defunct today. Education is democratized enough that anybody can get a decent one. The _real_ trouble is faced by that other class of students: those who are good at everything. They are the ones truly _lost_ because what should they study? They enjoy everything. They are _passionate_ about everything. Ironically, no one has an answer to that.  

IIITH (UGEE) is yet to announce its list of selected students. The same goes for IISER (though, I will not go there). IISc is offering a BSc programme (I have to take a clear stance latest by Sept 10th) and IITs are still a distinct league away. Why does it have to be so fucked up?     

## The question  

The question really boils down to this, **am I willing to turn down IISc, an irreversible decision, and sit for JEE Advanced thus exposing myself to the uncertainties of examination however hard I prepare and I may not even get IIT, or am I willing to let go of the security and money that a Silicon Valley MNC offers to a fresh IIT graduate and grind my ass through the rigor and intensity of IISc and the academia, in general?**  

I have to be analytical. 

## My skills, talents, passion  

Where do I see myself in the next 10 years? (I am not saying 20 years because I know the kind of restless monkey I am at heart. I'll probably be taking art courses and playing in rock bands by that time)  

I am restless. I'm not the kind who will sit through a project for days or months. If I set my mind to a task, I'll complete it, forgetting everything. Even food and water. **I want a profession that will complement this idiosyncracy**. (However, I am not _reckless_. I am meticulous in the work I undertake. I scrutinise everything. I read the manuals before I fly the airplane. I take calculated risks. Like a rational optimist).    

I want to startup. That is almost a certainty. I cannot grind myself through years of writing and reviewing papers while the world changes around me. I want to be there. I have set for myself a task of being a millionaire by the age of 20. So, yeah, the usual rules don't apply to me.   

## Pros and Cons of pursuing a BSc from IISc  

| Cons | Pros |
|:----|:----|
| Strictly a research institute | Gives the liberty to explore all the sciences | 
| Rigorous, grueling regime | I can use the free time to build my startup(s) _if I am even more hardworking_ | 
| Only a BSc degree. If this was a 5y BS-MS integrated, I wouldn't even be writing this post | A BSc degree from _IISc Bangalore_ | 
| Companies recruit generally only after MSc. That is, decent package (This is a last call option. I don't care about becoming an employee really) | &ndash; |  
| Not enthusiastic startup culture | &ndash; |  
| Institute geared towards MSc (not a con really) | I can use the MSc atmosphere to advance my knowledge and develop better ideas for startups |  
| &ndash; | Incredible research opportunities abroad | 
| Intense Tropical climate. RIP my skin. | Perhaps, I can compensate when I go to Europe? |  
| Some parts of the course redundant for me. Eg: Python 101 | &ndash; | 


## My foreign dream   

So, here's the thing.  India is not a country I resonate with. I am not like the people here, in terms of thinking, mindset, behavior, words. I don't like the place (but perhaps, I am baised based soley on where I live). I want to go to a place where people are enthusiastic about the delicate things of life. I am a romantic. And I want to grow. France is a promising candidate with its rich history of artists and musicians, its love for art. Followed by Germany for its history of great thinkers and designers. Scandinavian country but bruh, the climate. UK is fine too.   

I really want to settle aboard _comfortably_ (as in not struggle to make ends meet). That is a primary objective.  

## Parent issue  

Poor family. Anxious father. Skeptic mother. You get the point. They want me to become a money-generator for the family as soon as possible. Besides, they have outdated, outmoded opinions of markets, careers etc that are a queer amalgamation of the bits and pieces of opinions and ideas of other people, whatever they were told or whatever they heard (overheard?). They have no original thought and I am certain they do nothing to overcome their ignorance (which makes me _hate_ them so much). Overriding their desires is like being an egg falling to the ground. Even if they are wrong, I am gonna break. Because I cannot really find happiness in anything I do, unless the people I care about are happy.   

