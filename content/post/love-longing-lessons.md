---
title: "Love, Longing and Lessons"
date: 2021-06-29T10:48:47+05:30
---  

It's 7 in the morning as I'm writing this and glad that I survived. Forget all I ever said about love or girls. All the mean things. I was wrong.   

Today I experienced my first longing. Oh, now I so surely understand these stories, these poems, these people who came before me. Laila and Majnu, Romeo and Juliet. Oh, it all makes a fucking lot more sense now.  

I have imagined her lying close to me. Oh God, yes I have. Do you know how it feels like for every muscle in your stomach to cramp together? Making a little black hole and a gut-wrenching feeling of being sucked in. It isn't a very good feeling. It all makes me so dizzy for her.  

Longing isn't a very great feeling. The want to hold someone tight and close to your hearts and yet be aware that it is not possible. There might be other girls but none of them will be her. Yes, I'm talking about A. We never met in real life, though. I still haven't gotten over her.  

Forget everything I thought about her. Her "cons list". So what if she drinks a lot? _I still care about her._ I really do. I imagine her every night in bed with me. Oh fuck, this pains so bad.   

And her? I think she has moved on pretty well. YouTube fame and riches suits her best. As she pointed out on our last talk, she meets a lot of people (unlike me). She'll find someone. I pretty much go to Instagram just to check on her if she is happy. I think she is. Oh God, I am so mad at her. For sending me her pictures. For wishing me good night. For telling me she cared about me. For telling me she doesn't give everyone a chance but she thought she could give me one. I miss her.   

Here's what I learnt:  

1. Girls are weary of intimacy. You're privileged if she lets you in her heart.  
2. NEVER REFUSE what you're given. 
3. There is ALWAYS a second chance. But NEVER EVER a third.  
4. Watch your words. They really matter.  
5. Never speak ill of other girls. It's utterly bad taste and very ungentlemanly.  
6. Don't make her what she is not. She is not a panacea that will cure all your problems.  
7. It has never been about you. It is always about her.  
