---
title: "It's my birthday!"
date: 2022-01-08T13:20:35+05:30
tags:
categories:
---

I feel kinda good that I share my birthday with great men I admire. Stephen Hawking, Elvis Presley, David Bowie to name a few. Also, Galileo's death anniversary if that makes you any happy. It sounds so funny but that happens to be the only source of happiness for me today. Happiness in the most abstract sense of the word. Why?  

I'm in university. Far away from people I know (who are so few in number I can count them on my hands anyway). I feel so fucking alone. And not just physically but mentally too. I haven't come across another person who can resonate with my thoughts. Who has a feverish, almost insane thrist for knowledge just for the sake of it. A person who can truly hold my confidence. I know it's a stupid thought but one that I like to cherish sometimes.  

I spent my night in agony. I crumpled my blanket into a ball and held onto it for the night. I was trembling, convulsing all that. I don't know how to put it. I have increasingly begun to find ways and excuses to escape. I'm not gonna kill myself. I'm not that foolish. But I just don't feel validated enough. I don't know if what I'm doing is worth anything. Who do I make happy?   

Things have changed pretty quickly for me. I'm >1300 KM away from home in a totally different environment, culturally and otherwise and I feel alone. Not only from a lack of someone (which is pretty obvious and generic so nothing special) but from a lack of possibilities. (1) It also doesn't help having a nosy little roommate who I can barely connect to. We share plesantries because we both know we have to somehow stick together to not make it anymore worse. I'm sooo going to take the "Don't marry the first girl you see" lesson seriously. I think it would have been niver if I weren't alone on my birthday.    

I feel fucking pathetic. My parents have sacrificed so much to send me here and here I'm still not done with my "issues". I'm supposed to be strong and pragmatic. What the hell has happened to me? When did I start wanting to hold someone so close to my body? When did I start thinking letting someone enter my space would be a good idea? **When did I start wanting to care for someone so bad?** Where is my mind?   

It's the worst birthday ever. Nobody knows about it and I don't want them to. Because I'm not desperate to get phony "Happy Birthday"s from people I barely know and who clearly don't mean it. My minors begin from tomorrow. The weather is sulking and miserable. Overcast clouds, chilly winds, and uninvited drizzles. The sun isn't up. I hate such days. It feels like a sad day to be born.   

(1): But then I know such a relationship won't work out. It can be worked out by two _self-sufficient_ grown-ups. I cannot entrust another person with the responsibility of keeping me happy and expect them to do so. That's beyond pathetic. Relationships are for people who have steadied themselves. I'm still caught in my tornado of miseries. But maybe I'm wrong? Maybe, that person _would_ somehow make things easier? Less miserable. I like to believe they would. Atleast, it gives me something to hold on to.    