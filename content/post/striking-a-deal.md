---
title: "Striking A Deal"
date: 2021-07-27T07:40:33+05:30
tags: ['personal', 'speaking up']
categories:
---  

{{<toc>}}
## Destruction of dreams  

The pandemic has brought misery, death and sorrow. Collateral damage is often neglected or not given its due condolences in the face of death. Education has been one of those collateral damage in this scheme of things. It is true that loved ones have been lost and people have scurried to breathe. But in my opinion, **nothing is more destructive than the destruction of dreams**. People come and people go, but ideas lead the human race forward.    

If people think the pandemic is over (TPR is in a declining trend) they are so utterly mistaken. The impact of this pandemic will be felt when this generation of students will hit the workforce, when they will become leaders and assume control of the machineries of state. The system has failed to disseminate true education (yes, sure, an eye-wash). When this generation will be asked to take us forward, it is going to scurry for cover. Not to forget, the utter mental distress that these youngsters had to bear. They will show anti-socialisation behaviors, lack of empathy and kindness. The pandemic has had a far greater reaching impact than most of us are letting on. But I am not surprised. It is a habit of the human brain to deny what it doesn't like for as long as it can.   

## Lost some...

I missed my last year at school. Missed the challenges and fun of being the seniormost batch. Missed my teachers, friends, lessons, the school itself. **I lost those memories that should rightfully have been mine.** Pathetic online teaching conditions, inept examinations, and a general sense of failure on the adults part are some of the memories I have made. That was only the beginning. College and univ entrance examinations have been postponed indefinitely. A particular date is announced for reassurance and we (those who work hard) study diligently only to know the exam is further postponed. This has been the modus operandi for the last year. At this point, I don't care about college examinations at all. I am not even studying for them.    

Lockdowns have been imposed in staggered manners, unplanned and prolonged &ndash; marks of a panicked and inept government. It has translated to almost zero mobility at an age when I should be out there in the world, exploring it, experiencing it. But all I get to experience is the four walls of my room, my computer screen and my thoughts. This is what being imprisoned feels like. **No one can imagine my agony and what it feels like to lose that golden time of one's life that will never come again.**    

## ..gained some  

Appalled as I am, I am also grateful to the pandemic. _Yes, grateful_.  

The free time has allowed me to rest a little, to question a lot of things deeply, and to begin building the man I want to be and not what others want me to be. These two years I have read countless books, pondered ideas of great men, asked myself questions that I never had the time to ask (I was too occupied with grades and pleasing my parents) and finding answers to some of them. I have learnt web tech, dabbled in other aspects of tech, entrepreneurship, even earned bit of money by freelancing. I have learnt skills and asked questions that schools and parents have never bothered to teach or cajoled us to ask.   

I have had the privilege to broaden my horizons of learning like never before. I have gained more perspective in these ~17 months than I have in my 18 years of (pitiful) existence. I feel amazed and stunned and the only question I manage to ask myself is this: _Where were you all this time?_   

Personally, I am changing too. I am trying to build myself physically. I am learning a new language. I developed a taste for music. I realise I love it so much, I don't only want to enjoy it, I want to make it. What's surprising is I didn't even know I loved music. I'm glad I know now. I have stopped my habit of biting nails because I can't play my guitar with chewed nails. I have tried to let go of this before but failed each time. Its a game of incentives. To my brain, the madness of playing a score is more valuable than the transient euphoria (?) of nail-biting addiction. **Music is a madness and you're so lost if you don't agree.**  

## The deal  

Yesternight, I presented a deal to my father. I assured him a <500 rank in JEE-ADV in return for not perstering me, interfering with my day-to-day life, not making me feel cringed or pitiful at my existence, not being a Indian father, not being a miser. In short, not being a typical asshole for a father. I articulated my case and presented it to him. I explained how what he does is wrong. How his idea of measuring progress in terms of money saved (or rather shaved) from meagre transactions like groceries are flawed. How progress should be measured in units of time saved. For eg: If it is justified to save time for a certain sum, than that time can be utilised to gain more than what was spent (Not earning money _per se_, but it can be used to spend time with family and consolidate precious filial bonds). **It is time that is truly scarce, not money.** My sister was smirking in bed all the time listening to me. She is often refused freedom on grounds of being a female. I know. I hate my household.       

I am just generally glad I could speak up to my father. I am smarter, disciplined and more responsible than most people. I want to prove it.   

_No time to waste._  
~b.l.

