---
title: "Cause and Effect"
date: 2021-07-13T18:33:36+05:30
---  

Sometimes it is wise to take account of one's situation.  

> "The man who has a _why_ to live, can manage almost any _how_.  
<cite>&ndash; Neitzsche</cite>  

How long can a man manage without a reason to live? Yes, I feel utterly defeated in all spheres of my existence. I wake up each morning not excited about what the day holds for me but shudder at how I am going to survive that day. It has been happening for a week now and I know something is not okay.   

It's 18:52 and Sia's "Salted Wound" is playing. The distinct vocalisation as instrumentation on this song reminds me of Elisabeth Fraser, the soprano on Cocteau Twins, the first band I became a fan of. Oh, now it's Emily Watts' "La Vie En Rose". I am alone at home.   

Nights are mostly insomniac. Incoherent and irregular. The last thing I need in this crucial week. College entrace exams begin from Friday. Night before yesterday, father was abusing my little sister. I couldn't take it so I stated the facts about us, him and our family. Sure enough, he didn't talk yesterday and frankly I don't care. If you can't find a job for 7 goddamn years then you ought not to have such ego too. He thinks he's the only one "struggling". What about my dreams? Dad? Ever asked me? I have bored all that, his insults, the missed opportunities with patience but there is a limit I guess. Yesternight, I planned to sleep early so as to rise early today and attempt to study. He shouted shit from 11 till past midnight.  

I am a sensitive sleeper. So, once my sleep window passed, all I was left with was insomnia and a terrible headache. Huh, so much planning for this. I don't believe in God as a "being". I believe in the web of events and actions that effect each other in bewildering and myriad of ways and of which we have no control. I believe in these invisible causes and effects which topple and tumble us at will without remorse, joy or hesitation.   

Yesternight, I was crying. I was crying at my condition, my destiny. This crumbling cage I live in, cramped by people I fail to understand, surviving on the barest of essentials, pathetic "lifestyle" and mountains of stakes and responsibilities that I am reminded of every fucking moment. But do you know what I was really crying about? I was crying about the future. I was crying at my helplessness. I am anxious. I am despaired. Kids grow up and look back at times with their parents, talk about things they learn from them. I guess, these will be my memories of my parents. How long?
