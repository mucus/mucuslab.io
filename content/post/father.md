---
title: "Dear Father"
date: 2021-04-29
---

Dear Father,\
\
I am not dumb. I am not prodigal. I understand poverty better than you
do. For you may have experienced it when you are older and wiser, with
white hair on your beard, I have experienced it since I was born, since
I was naked and helpless. I know it before I could learn to act. I can
go on and on but this is not the point here. Besides, I have become
accustomed to poverty.\
\
I understand your concern very well. I have read words of great men. I
have pondered ideas of visionaries and I know it in my heart that I\'m
not made for the usual way. Do you understand how rare is that? Do you
realise how lucky I am to figure it out before it\'s too late? I have
died each time I have seen opportunities fly by my face. But I have bore
it with patience. I have stayed quiet. I have silently drank the
bitterness. I still do. And for all that you should know, it hurts.\
\
You say \'look at you. When did this new ghost possess you?\' Do you
have any idea how it feels like to hear that from the one person I
admired? (Yes, admired because I don\'t admire you any more). Yes, yes
fucking yes. It\'s a ghost that has possessed me. And you are wrong.
This ghost didn\'t possess you or any of my forefathers. This ghost that
can topple civilizations, bring revolutions. Do you want to know why?
Because it didn\'t find any of you worthy. Worthless are your beliefs
that you refuse to let go. For where have they led you? To poverty and
decline. Who speaks the name of my grandfather with veneration today?
And do you want to know, why I\'m possessed? Because I believe in the
ghost. Real is what we believe.\
\
Am I sure of my future? Of my success? These lofty words that I use. Is
the man I\'m trying to build complete? NO! No. No. No. I\'m as anxious
and vulnerable as any boy my age. I\'m as lost as they are. But if
you\'re thinking about it that way, you\'re again wrong. In these days
locked in our house, I have found what I love. I have seen all that I
have missed. Such a blithering idiot I have been to pretend the world
never existed and bury myself in books. Alas, if it worked that way for
shamefully I have become this dull being who can\'t even talk to people.
And now when I ask myself, what am I good for? Silence is the only
answer I get. I do not detest or denounce your ideas and ways for you
are indeed wiser. I only wish you were sensible enough to consider my
emotions and opinions and let them coexist with yours. And if not
anything, atleast respect them.\
\
Is it my fault if you never truly fell in love with something in your
life?\
\
Do you realise how plain wrong you are, to try to extinguish this fire
in me? Don\'t you have even an ounce of culture in you? The thing that
made me like you. For now, I\'m sure I hate you. With all my heart.\
\
Do you realise how badly it pains when you constantly express your
disapproval in a low murmured tone? You don\'t scream or shout but that
which is do is enough to rip my heart. And I hate you for that.\
\
You say I ought to bring back money (glory you say but I know it\'s money
that you seek). But do you know how? You had your time and you expended
it. Now, you have no right to judge how I choose to do it. And it sucks
when you do. All you ever talk about is your investment in me and not
me. Have you ever asked what I wanted to do and not turn it into
something profitable and safe and secure for my pitiful existence?\
\
Greatness doesn\'t come from doing great things. It comes from doing
simple things in a great way. I wish you could understand that. I do not
want or expect you to be like my friend\'s fathers. I don\'t expect you
to be cool. I have known you enough to know what you value in life. I\'m
fine with it. But what hurts is when you mumble when our values don\'t
align.\
\
I genuinely thought you\'d be glad to know that your son is trying to
come to terms with the world. To find his way on his own. But you\'re so
constrained. Apart from your share of 23 chromosomes, I think I hold
little common with you. We get to choose our friends, lovers and in a
twisted way, children but alas, we don\'t get to choose our parents. And
I think that is unfair.\
\
Do you have any idea what these 14 months have been like for me? When I
was supposed to begin doing what all I have missed. Sports, music,
laughter, happiness and most importantly freedom. But look, what has
happened to the plans. Do you understand under what pressure I am in?
Have you ever asked me how I was doing? When was the last time I had
truly demanded something solely for my pleasure? And better still, how
many times have you actually brought them? I do not see me or you. I see
the long line of ancestry of which I have descended. My plan has always
been to make it glorious again. If you think it can be done with the
education you think of, you\'re simply wrong. You don\'t know. The world
is changing like never before. It will be a pity if we don\'t. Remember
the time you told me \'who is holding you? You are free to do whatever
you want\'. I think you\'re a liar. You pin me down. You make me sick.\
\
&ndash; Your son   
Apr 29, 2021
