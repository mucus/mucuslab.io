---
title: "WTF am I doing?"
date: 2022-02-01T14:10:36+05:30
tags:
categories:
---  

Have you ever had that sense of "not belonging"? When you have held onto a particular idea or notion or person for too long, rejecting all other opportunities that came your way, resting assured in your belief, no, your trust in that idea and when one day, it finally makes itself evident that that idea or notion or person is wrong, flawed, or incompatible? When you have this _intense_ emotion of rage and regret building up? When you just can't stop thinking "_what if_"? When you feel utterly lost because you were betrayed by your own self and who can you trust if you can no longer trust yourself? Yes, I'm in that phase.  

It is particularly easy to cheat in online exams and it is happening in our uni. People who know nothing about mechanics have scored full marks. All this is making me really nervous because I want to get a department change (I cannot study textiles for 3 freaking years, come on, who am I kidding?) and that is based on who scores more at the end of first year. Is it _really_ justified that I'm not cheating? What am I trying to prove? And who am I trying to prove it to? I'm so fucking tired of trying to be Nietzsche's higher man.   

What is the good in being good? UGH. I _love_ Computer Science. And you know all about it. But what if some dishonest crook gets higher grades than me by using unfair means and gets that major? Is being good justified in this case? I will be freaking trapped in textiles for 3 years. And I know it in my heart that I'm going to hate myself every second thinking about those people who got better grades than me and who got all these internships and research opportunities at international univs. I know it in my heart, I'm not going to "study" textiles with the diligence and sincerity I give to CS. I feel so pathetic. I hate myself.   

I have always found it nobler to sacrifice myself for the sake of others and I have begun to feel how stupid I am. I have found this romantic quality of self-righteousness in helping others and I feel miserable because people don't give a shit. People never like me. People never like anybody. People are selfish. And I'm not. And that is what makes me heartbroken. Increasingly, I have begun to admit to myself, I'm hanging around with the wrong crowd.  

One of my seniors told me to "be unreasonable". In a population, when cheating in exams seems the most reasonable thing to do, then be unreasonable. I'm so fucking anxious. I just wanted CS major or something related to it. The senior who sent me this lovely quotation by G. B. Shaw is a EE major himself so nevermind.  

> "The reasonable man adapts himself to the world: the unreasonable one persists in trying to adapt the world to himself, Therefore all progress depends on the unreasonable man."   
~G. B. Shaw   

Yes, I'm unreasonable but what good is that? Am I digging a grave myself to sleep in?  

I realise now how one might want to reach out and talk to another human being. University is a cauldron of many outstanding peers and exceptional talents and it is so _damn_ easy to get depressed and develop inferiority complex. Am I standing on the verge of that abyss? How can I ever be sure of what I'm doing is right?   

I feel like forcing my head into a pillow and screaming myself to death. Or beat the shit out of drums until my hands bleed. Do I want a channel to vent my despair? Do I want to end everything and get rid "of this mortal coil" as Shakespeare puts it? What do I fucking want?
