---
title: "Leaving for university & a new friend"
date: 2021-12-15T16:12:10+05:30
tags:
categories:
---

I have no idea how to express it but I have found deeper meanings of love.   

My sister and I are worlds apart, though she's only a year younger. Her thoughts and ideas, outlooks, and "basic philosophy" of life, that is, how she approaches opportunities, challenges, or failure, are diametrically opposite to mine. We never used to talk, forget about "getting to know each other" and yet we were siblings, born of the same mother. I never knew what was going on with her neither did she ask about mine. We were existing together in one house. That was it.    

But the past year has brought a monumental change to our perceived images of one another. I feel so strange but I have _grown close to her_. Let me repeat it to reassure myself: I have grown close to another human. If you ever get to be in my place (and good luck btw), you'll understand how terrifying the prospect of trusting another person is to me. And, yet I have not only known her better but have genuinely cared for her. I have asked her how her day was and stuff like that. We have discussed endlessly about movies we watch (though, she's not as crazy about cinema as I am), especially the sitcom series "Friends". I had to stop myself from hugging her on _several_ occassions. Why? (See next paragraph). Surprisingly, I have found a friend when I felt there was no one for me and that means a lot.     

Today, it has been decided that I will be leaving for university by next week. I felt such an indescribable strong urge to hug my sister that I had to fight it (For a moment, I thought she wanted to hug me too but I maybe wrong) Why? Because we haven't ever hugged. And what would her reaction be if I did? Will she return it or shriek in horror? Perhaps I'll never know. It is this lack of communication and understanding that I abhor about my family ways. While handing things, if ever my skin brushes against hers, she repulsed back with a jolt. Why? That's what she's been taught. To care about someone is forbidden in our house. Why? Because the elders believe the purpose of a person is to be born, toil, bear kids, suffer, and return to the mud. We don't exchange presents on birthdays. Or even just a small gift to show that we care about each other.     

I won't say my family is loveless. It's just different than what I have pictured it and sometimes I have a hard time finding that love and sometimes it doesn't exist.    

I have been a strong boy this year and I think my sister will be an integral part of my life from now on.       