---
title: "Dear Father 2"
date: 2021-09-17T10:07:47+05:30
tags:
categories:
---

Dear Father,  

If other kids have a father like you, they'll end up doubting their own talents and skills. Indeed, their very existence.

If I can have my way, I don't ever want to talk to you because you're a man who can bend words to suit his needs and I hate such ignominious and sly actors. You were swift to tell twice to mother how I said "I knew more than you". But did you tell her in what context and under what circumstances I said that? Is it desperation at the heralding of that time when you won't take decisions for me?
And in faith, I _do_ know more than you. I know more than what you knew at 18. At the hazard of bragging, I know more than what most 18 year adolescent is _expected_ to know.

Let me be frank and wholehearted like I have always been. It does sting to assimilate the fact that I won't appear for JEE advanced, the single most-revered examination in our country. It makes me feel guilty, not nauseous. As if I have somehow cheated everybody else who are clamoring and distressing over securing a respectable college.

But come to think of it, I haven't cheated. Would you call it cheating to spend those nights in woe and agony? The daily irritating cooking vapours and the subsequent frantic scurrying out of the house, leaving behind any work at hand no matter how important? The frequent motor failure and water conundrum? The call and cries and a thousand laughs of other people's conversations that uninvited and unsuspectingly swim into our rooms and ears, making us unintended culprits of eavesdropping? The sharp pain in my legs that result from walking all the way through to the library and back just because there is no place to study at home, and let's not kid ourselves with projecting walking like a pig as some kind of elevated, poignant ritual? The constant bumping into each other for lack of space to move around, forget about living, no space to just freely move around? The every morning insults and rebukes that I silently bear for being myself? The subtle venomous words that slice through my heart but alas cannot kill me that spews out the mouth of my dear mother for when I try to find who I am? I'll stop here because I have other work.

My seat at IISC is a fruit of these and other hardships I have bore. And what would you know about me? If you're worried about _my_ future then don't be (But ask yourself clearly, what you're worried about. Me or you). I'm learning and I'm satisfied. I wake up every morning invigorated with the thought of learning something I love and I'm fine with
that. 

&ndash; b.l.  
Sep 16th, 2021  